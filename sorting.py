import sys

for line in sys.stdin.readlines():
    if ("c" in line or "d" in line):
        print line.replace("\n", "")
    else:
        print " ".join(map(str, sorted([i for i in map(int, line.split()) if i != 0 ])+[0]))
