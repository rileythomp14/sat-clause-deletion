#!/bin/bash
#SBATCH --time=12:00:00
#SBATCH --account=def-vganesh
#SBATCH --mem=10g
#SBATCH --mem-per-cpu=8192M
#SBATCH --cpus-per-task=1
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=jr3thomp
#SBATCH --output=${1}.out
./glucoseLBD -cpu-lim=36000 -certified -certified-output=${1}.drat ${1}
sleep 10

