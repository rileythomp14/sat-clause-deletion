from __future__ import print_function

import sys
import csv

proofFile = open(sys.argv[1], "r")
coreFile = open(sys.argv[2], "r")
csvFile = open(sys.argv[3], "w")

csvWriter = csv.writer(csvFile, delimiter=',')

coreClauses = [line.rstrip() for line in coreFile.readlines() if not "c" in line and not "d" in line]
print(coreClauses)

csvWriter.writerow(["core", "len", "lbd", "clause"])
while True:
    line = proofFile.readline().rstrip()
    if line is None or line == "":
        break
    LBD = -1
    if "d" in line:
        continue
    if "c" in line:
        if "LBD" in line:
            LBD = int(line.split(" ")[-1])
            line = proofFile.readline()
        else:
            continue
    isInCore = line in coreClauses
    #print("line: ", line)
    try:
        clause = map(int, line.split(" "))[:-1]
    except:
        sys.stderr.write(line)
    if len(clause) == 0:
        break

    csvWriter.writerow(["T" if isInCore else "F", len(clause), LBD, clause])
csvFile.close()
