#!/bin/bash
#SBATCH --time=12:00:00
#SBATCH --account=def-vganesh
#SBATCH --mem=10g
#SBATCH --mem-per-cpu=8192M
#SBATCH --cpus-per-task=1
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=jr3thomp
#SBATCH --output=%x.out
python2 sorting.py < ${1%core}drat >> ${1%core}drat.sorted
python2 sorting.py < ${1} >> ${1}.sorted 
sleep 10

